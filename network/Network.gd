extends Node

signal host_succeeded
signal join_succeeded


func _ready():
	multiplayer.peer_connected.connect(self._peer_connected)
	multiplayer.peer_disconnected.connect(self._peer_disconnected)
	multiplayer.connected_to_server.connect(self._connected_to_server)
	multiplayer.connection_failed.connect(self._connection_failed)
	multiplayer.server_disconnected.connect(self._served_disconnected)

func host(port: int = Settings.port) -> bool:
	var server := WebSocketServer.new()
	if server.listen(port, PackedStringArray(), true) == OK:
		get_multiplayer().multiplayer_peer = server
		print("hosting on port: ", port)
		host_succeeded.emit()
		return true
	else:
		print("could not host")
		return false

func join(address: String, port: int = Settings.port) -> bool:
	var client := WebSocketClient.new()
	if client.connect_to_url(get_url(address, port),
			PackedStringArray(), true) == OK:
		get_multiplayer().multiplayer_peer = client
		return true
	else:
		print("failed to connect")
		return false

func _peer_connected(id: int):
	print("%d connected" % id)
	rpc('ping', 42)

func _peer_disconnected(id: int):
	print("%d disconnected" % id)

func _connected_to_server():
	join_succeeded.emit()
	print("connected to server")

func _connection_failed():
	print("connection failed")

func _served_disconnected():
	print("server disconnected")

func get_url(address: String, port: int) -> String:
	# ws://my.address:1337
	return "ws://%s:%d" % [address, port]

@rpc(any_peer)
func ping(x: int):
	print("I've been pinged with %d by %d" % [x, multiplayer.get_remote_sender_id()])
