extends VBoxContainer

@export
var connection_menu_path: NodePath
@export
var connection_status_menu_path: NodePath

func show_unconnected_mode():
	show_menu(connection_menu_path)
	hide_menu(connection_status_menu_path)

func show_connected_mode():
	hide_menu(connection_menu_path)
	show_menu(connection_status_menu_path)

func show_menu(path: NodePath):
	var menu := get_node_or_null(path) as Control
	menu.show()

func hide_menu(path: NodePath):
	var menu := get_node_or_null(path) as Control
	menu.hide()
