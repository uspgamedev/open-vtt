extends MarginContainer

signal back_pressed


func _ready():
	Network.join_succeeded.connect(self._on_network_join_succeeded)


func _on_back_button_pressed():
	back_pressed.emit()


func _on_connect_button_pressed():
	var address : String =\
			$Panel/Contents/Columns/VBoxContainer2/HostAddress.text
	var port : int = $Panel/Contents/Columns/VBoxContainer2/Port.text.to_int()
	Network.join(address, port)


func _on_network_join_succeeded():
	get_tree().change_scene("res://ui/session_view/Session.tscn")
